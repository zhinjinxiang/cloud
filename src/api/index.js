import Http from "./http";
import axios from "axios";

// 请求uri地址
// import config from '../config/uri'

/* export const getNowPlayingFilmList = ()=>{
  return http.get(url)
} */

// const allowToken = ['/login']

// 请求拦截器
// axios.interceptors.request.use(config => {
//   // config.baseURL = 'http://localhost:8081'
//   config.timeout = 3000
//   // 除登录业务以外都要携带token
//   if(!allowToken.includes(config.url)){
//     config.headers['token']  =  store.getters.getToken
//   }
//   return config
// }, error => Promise.reject(error)
// )

class Api extends Http {
  // 正在热映
  // getNowPlayingFilmList({ cityId, pageNum = 1, pageSize = 10 }) {
  //   let url = config.getNowPlayingFilmList + `?cors=T&cityId=${cityId}&pageNum=${pageNum}&pageSize=${pageSize}`
  //   return this.get(url)
  // }
  getCaptcah({ phone }) {
    let url = "http://localhost:3000/captcha/sent" + `?phone=${phone}`;
    return this.get(url);
  }
  getUserInfo({ phone, password }) {
    let url =
      "http://localhost:3000/login/cellphone" +
      `?phone=${phone}&password=${password}`;
    return this.get(url);
  }
  getUserList(uid) {
    let url = "http://localhost:3000/user/playlist" + `?uid=${uid}`;
    return this.get(url);
  }
  getUserGrade() {
    let url = "http://localhost:3000/user/level";
    return this.get(url);
  }
  getSongList(uid, type) {
    let url = "http://localhost:3000/user/record" + `?uid=${uid}&type=${type}`;
    return this.get(url);
  }
  getSingListDetail(id) {
    let url = "http://localhost:3000/playlist/detail" + `?id=${id}`;
    return this.get(url);
  }
  getAllSong(id) {
    let url = "http://localhost:3000/song/detail" + `?ids=${id}`;
    return this.get(url);
  }
  getMv(limit = 20) {
    let url = `http://localhost:3000/mv/first?limit=${limit}`;
    return this.get(url);
  }
  getkey(key) {
    let url = `http://localhost:3000/search/suggest?keywords=${key}`;
    return this.get(url);
  }
  getSingers({ type = -1, area = -1, initial = "b" }) {
    let url = `http://localhost:3000/artist/list?type=${type}&area=${area}&initial=${initial}&limit=20`;
    return this.get(url);
  }
  getVideo(id) {
    let url = `http://localhost:3000/mv/url?id=${id}`;
    return this.get(url);
  }
}

export default new Api();
