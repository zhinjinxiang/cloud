// 导入网络请求
import axios from "axios";
import store from '../store'
// 创建一个类并导出  基础类
export default class Http {
  // 构造方法 在类实例化时，第1个被执行  可以进行初始化操作
  constructor() {
    this.axios = axios;
    // 全局统一配置
    // axios.defaults.baseURL = "https://api.iynn.cn/film/api/v1";
    // const allowToken = ['/login']
    // axios.interceptors.request.use(
    //   (config) => {
    //     // config.baseURL = 'http://localhost:8081'
    //     config.timeout = 3000;
    //     // 除登录业务以外都要携带token
    //     if (!allowToken.includes(config.url)) {
    //       config.headers["token"] = store.getters.getToken;
    //     }
    //     return config;
    //   },
    //   (error) => Promise.reject(error)
    // );
    // 响应拦截器
    axios.interceptors.response.use(
      (response) => {
        return response.data || response;
      },
      (error) => {
        return Promise.reject(error);
      }
    );
  }
  /**
   * get请求
   * @param {string} url
   * @param {json} headers
   * @return Promise
   */
  get(url, headers = {}) {
    return this.axios.get(url, {
      headers,
    });
  }
}

// export default new Http;
