export default {
  userInfo: (state) => state.getUserInfo.userInfo,
  singList: (state) => state.getUserInfo.singList,
  status: (state) => state.getUserInfo.status,
  userGrade: (state) => state.getUserInfo.userGrade,
  songList: (state) => state.getUserInfo.songList,
  singListDetail: (state) => state.getUserInfo.singListDetail,
  allSong: (state) => state.getAllSong.allSong,
  getId() {
    return sessionStorage.getItem("uid");
  },
  getToken(state) {
    return state.token || sessionStorage.getItem("token");
  },
  areaActive: (state) => state.singer.areaActive,
  typeActive: (state) => state.singer.typeActive,
  dataList: (state) => state.singer.dataList,
  historyList: (state) => state.history.historyList,
};
