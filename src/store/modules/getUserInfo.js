import api from '../../api'
export default {
  state: {
    token:'',
    userInfo: {},
    singList: {},
    status: false,
    uid: '',
    userGrade: '',
    songList: [],
    singListDetail: [],
    allSong:[]
    // singList: {},
  },
  mutations: {
    verify ( state, data ) { 
      // console.log(data);
      if ( data.code == 200 ) { 
        state.userInfo = data
        state.status = true
        state.token = data.token
        // console.log(state.userInfo);
        sessionStorage.setItem('userInfo',state.userInfo)
      }
    },
    getSingList ( state, data ) { 
      state.singList = data.playlist
      // console.log( state.singList );
      sessionStorage.setItem('singList',state.singList)
    },
    getUserGrade ( state, data ) { 
      state.userGrade=data.data.level
    },
    getSongList ( state, data ) { 
      state.songList = data.weekData
      // console.log(state.songList);
    },
    getSingListDetail (state,data) { 
      state.singListDetail=data.playlist
    },
    
    logout ( state ) { 
      state.status = false
      // this.$storage.clear()
      sessionStorage.clear()
      state.userInfo = {}
      state.singList = {}
      state.uid = ''
      state.songList = []
      state.token=''
    }
  } ,
  actions: {
    // async getCaptcah ( { commit }, data ) { 
    //   let res =await api.getCaptcah( data )
      
    // },
    async verify ( { commit }, data ) { 
      // console.log(data);
      let res = await api.getUserInfo( data )
      // console.log(res);
      sessionStorage.setItem('token',res.token)
      commit('verify',res)
    },
    async getSingList ( { commit},data) { 
      let res = await api.getUserList( data )
      // console.log(res);
      commit( 'getSingList', res )
      sessionStorage.setItem('uid',data)
    },
    async getUserGrade ( { commit},data) { 
      let res = await api.getUserGrade( data )
      commit('getUserGrade',res)
    },
    async getSongList ( { commit }, data ) { 
      // console.log(data);
      let res = await api.getSongList( data, 1 )
      // console.log(res);
      commit('getSongList',res)
    },
    async getSingListDetail ( { commit }, data ) { 
      // console.log(data);
      let res = await api.getSingListDetail( data )
      console.log(res);
      commit('getSingListDetail',res)
    },
    
  }
}