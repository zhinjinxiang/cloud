export default {
    state:{
       historyList:[],
    },
    mutations:{
        updateHistory(state,data){
            state.historyList.push(data);
            localStorage.setItem('history',JSON.stringify(state.historyList));
        },
        getHistory ( state, data ) {
            if (JSON.parse(localStorage.getItem('history'))){
               state.historyList=JSON.parse(localStorage.getItem('history'));
           }
           
        }
    }
}