import api from "@/api"
export default {
    state: {
        dataList: [],
        areaActive: 0,
        typeActive: 0,
        areaId:-1,
        type:-1
      },
      mutations:{
        // 只负责更新当前激活的索引号
        setState(state,data){
           if(data.val=="area"){
            state.areaId=data.id;
            state.areaActive=data.index;
           }else{
             state.type=data.id;
             state.typeActive=data.index;
           }
        },
        getSingers(state,data){
            state.dataList=data;
            // console.log(...state.dataList);
        }
      },
      actions:{
        async getSingers(store, obj) {  
            let type = null;
            let area = null;
            if (obj.val === "area") {  
              area = obj.id;
              type=store.state.type;
              console.log(area,type);
            } else {
              type = obj.id;
              area=store.state.areaId;
              console.log(type,area);
            }
            let {artists} = await api.getSingers({type, area});
            console.log(artists);
            // console.log(artists);
            store.commit('getSingers',artists)
          },
      }
}