import api from '../../api'
export default {
  state: {
    allSong:[]
  },
  mutations: {
    getAllSong ( state, data ) { 
      // console.log(data);
      state.allSong.push(data.songs[0])
      // console.log(state.allSong);
    },
    clearData ( state ) { 
      state.allSong=[]
    }
  },
  actions: {
    async getAllSong ( { commit }, data ) { 
      let res = await api.getAllSong( data )
      commit('getAllSong',res)
    }
  }
}