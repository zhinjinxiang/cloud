import Vue from 'vue'
import Vuex from 'vuex'
import getUserInfo from './modules/getUserInfo'
import getAllSong from './modules/getAllSong'
import getters from './getters'
//lj
import singer from "./modules/singer"
import history from "./modules/history"

Vue.use(Vuex)

const store = new Vuex.Store( {
  modules: {
    getUserInfo,
    getAllSong,
    singer,
    history
  },
  getters
} )

export default store
