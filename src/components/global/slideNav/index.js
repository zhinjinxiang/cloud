import Vue from 'vue'
import slideNav from './index.vue'

Vue.component('slideNav', {
  render: h => h( slideNav )
} )
