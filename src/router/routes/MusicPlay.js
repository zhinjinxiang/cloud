export default {
    path: "/musicplay/:id",
    name: "musicplay",
    component: () => import(/*webpackChunkName: 'ktv' */'@/views/musicPlay')
    , props: true
}