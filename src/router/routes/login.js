//登陆页面的路由
export default {
    path:"/login",
    name:"login",
    component: () => import(/*webpackChunkName: 'ktv' */'@/views/login' ),
    meta: {
        allow:true
     }
}