export default {
    path:"/userinfo",
    name:"userinfo",
    component: () => import(/*webpackChunkName: 'userinfo' */'@/views/own/userInfo')
}