// 播报路由模块
export default [
   {
    path:"/podcast",
    name:"podcast",
    component: () => import(/*webpackChunkName: 'podcast' */'@/views/podcast'),
    children: [
        {
          path: 'con1',
          name: 'con1',
          component: () => import(/*webpackChunkName: 'con1' */'@/views/podcast/con1'),
          // children:[
          //   {
          //   }
          // ]
        },
        {
          path: 'con2',
          name: 'con2',
          component: () => import(/*webpackChunkName: 'con2' */'@/views/podcast/con2'),
        },
        {
            path: 'con3',
            name: 'con3',
            component: () => import(/*webpackChunkName: 'con3' */'@/views/podcast/con3'),
          },
        // 重定向
        { path: '/podcast', redirect: 'con1' }
      ]
   }
]
