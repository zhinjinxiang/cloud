import Vue from 'vue'
import VueRouter from 'vue-router'
import find from "./routes/find"
import podcast from "./routes/podcast"
import own from "./routes/own"
import ktv from "./routes/ktv"
import cloudVilage from "./routes/cloudvillage"
import search from "./routes/search"
import musicplay from "./routes/MusicPlay"
import login from "./routes/login"
// import store from '../store'
import userInfo from './routes/userInfo'
import latest from './routes/latest'
import singListDetail from './routes/singListDetail'

//lj
import bangdan from "@/components/global"
import showMv from "./routes/showMv"
// 路由入口文件
Vue.use(VueRouter)

const routes = [
  find, ...podcast, own, ktv, cloudVilage, search, musicplay,login,userInfo,latest,singListDetail,bangdan,showMv,
  {
    path: '/',
    redirect: "/login",
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
