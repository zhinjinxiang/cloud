import Vue from 'vue'
Vue.filter('Tostr', (value) => {
    if (value.length > 7) {
        return value.substring(0, 7) + "..."
    } else {
        return value
    }

})

Vue.filter('Num', (value) => {
    return (value / 10000).toFixed(0)
})