import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import api from "./api"
import validate from './validate/words'
import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload, {
  loading: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1607240299050&di=9210495e081f2825cb8c853e7f337e82&imgtype=0&src=http%3A%2F%2Fhbimg.huabanimg.com%2F47bfa2df6ad2db8fc84eb52770b566cde15b8e5e20c94-ouPK1z_fw658",
});

Vue.prototype.$http = axios
const bus = new Vue();
Vue.prototype.$bus = bus

Vue.prototype.$api = api


import storage from 'good-storage'

//将axios挂载到全局
// import api from './api'
// Vue.prototype.$api = api

// import 'lib-flexible/flexible'
//引入全局的侧边导航
import "@/components/global/slideNav"
Vue.config.productionTip = false

Vue.prototype.$storage=storage

new Vue( {
  store,
  router,
  render: h => h(App)
}).$mount('#app')
